import React from "react";
import "./Cards.css";
import CardItem from "./CardItem";

function Cards() {
  return (
    <div className="cards">
      <h1>Check out these awesome travel clips!</h1>
      <div className="cards__container">
        <div className="cards__wrapper">
          <ul className="cards__items">
            <CardItem
              src="videos/video-4.mp4"
              text="Qui dolore sint laboris enim adipisicing labore aliquip anim dolore sunt fugiat irure sint dolore."
              label="Sanctuary"
              path="/services"
            />
            <CardItem
              src="videos/video-5.mp4"
              text="Ea adipisicing ipsum ut labore et occaecat est proident commodo."
              label="Adventure"
              path="/services"
            />
          </ul>
          <ul className="cards__items">
            <CardItem
              src="videos/video-6.mp4"
              text="Sit non culpa esse proident ipsum."
              label="Adventure"
              path="/services"
            />
            <CardItem
              src="videos/video-7.mp4"
              text="Culpa amet ullamco magna minim aliquip velit cillum nostrud sunt do non consectetur."
              label="Escape"
              path="/products"
            />
            <CardItem
              src="videos/video-8.mp4"
              text="Eu do elit commodo Lorem."
              label="Sanctuary"
              path="/sign-up"
            />
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Cards;
